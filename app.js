'use strict';

// [START app]
const express = require('express');
var bodyParser = require('body-parser');

var session = require('express-session');
var cookieParser = require('cookie-parser');
const app = express();


//To parse URL encoded data
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser());
app.use(session({
  secret: "parallel-node",
  resave: true,
  saveUninitialized: true
}));

//To parse json data
app.use(bodyParser.json())
app.set('view engine', 'pug');
app.set('views','./views');
app.enable('trust proxy');

app.use(express.static(__dirname + '/public'));

var mysql = require('mysql')
var mode   = process.env.MODE;
var connection;
if (mode === "DEV") {
  connection =mysql.createConnection({
    host: '127.0.0.1',
    user     : 'root',
    password : 'root',
    database : 'pizza',
  });
  connection.connect()

} else {
  connection = mysql.createConnection({
    socketPath: '/cloudsql/pizzaparallel-php:asia-northeast1:pizza-para',
    user     : 'root',
    password : 'root',
    database : 'pizza',
  });
  connection.connect()

}


app.get('/', (req, res) => {
  let cart = req.session.cart || []
  req.session.cart = cart
  res.render('index', { user: req.session.user, cart: req.session.cart })
});


app.get('/logout', (req, res) => {
  req.session.user = null
  req.session.cart = []
  res.render('index', { user: req.session.user, cart: req.session.cart });
});

app.get('/product', (req, res) => {
  let u = req.session.user
  let cart = req.session.cart || []
  req.session.cart = cart
  res.render('product', { user: req.session.user, cart: req.session.cart })
});

app.get('/product1', (req, res) => {
  let u = req.session.user
  let cart = req.session.cart || []
  req.session.cart = cart
  res.render('product1', { user: req.session.user, cart: req.session.cart })
});

app.get('/checkout', (req, res) => {
  let u = req.session.user
  if (!u) {
    res.redirect('/login') 
    return;
  }
  let cart = req.session.cart || []
  req.session.cart = cart

  res.render('checkout', { user: req.session.user, cart: req.session.cart })
});
let user;
app.post('/checkout', (req, res) => {
  // TODO 
  // add order to db
  console.log(req.session.cart)
  var totalPrice = req.session.cart.reduce((acc, curr) => acc + curr.price, 0)
  let cart =         req.session.cart
  var sql = `INSERT INTO \`pizza\`.\`order\` (account_id, total) VALUES (${user.id},${totalPrice})`;
  connection.query(sql, function (err, result) {
    if (err) throw err;
    let id = result.insertId;
    

    cart.map((item) => {
      var sql = `INSERT INTO \`pizza\`.\`pizza\` (order_id, crust, size, menu, topping, price)`;
      sql += `VALUES (${id},'${item.crust}','${item.size}','${item.menu}','${item.topping.join(',')}',${item.price})`;
      connection.query(sql, function (err, result) {
      });
    })
    req.session.cart = null
  })
  req.session.cart = null
  res.render('checkout', { message: 'Checkout complete, Total $' + totalPrice +'.00' , cart: [], user: req.session.user})
});

app.get('/order', (req, res) => {
  let u = req.session.user
  if (!u) {
    res.redirect('login')
    return;
  }
  let cart = req.session.cart || []
  req.session.cart = cart
  res.render('order', { user: req.session.user, cart: req.session.cart });
});

const mapPrice = (item) => ({
  'Crispy Thin': 70,
  'Cheese Crust': 70,
  'Extreme Sausage': 70,
  'Extreme Cheese Crust': 70,
  'Pan': 70,
  'Small': 0,
  'Medium': 0,
  'Large': 0,
  "Hawaiian": 90,
  "Seafood Deluxe": 100,
  "Seafood Cocktail": 100,
  "Super Deluxe": 90,
  "Chicken Trio": 90,
  "Shrimp Cocktail": 90,
  "Tropical Seafood": 100,
  "Double Pepperoni": 70,
  "Bacon Super Delight": 90,
  "BBQ Chicken": 19,
  "Bacon Bits": 19,
  "Crab Stick": 19,
  "Garlic Butter Chicken": 19,
  "Pepperoni": 19,
  "Prawn": 19,
  "Seafood": 19,
  "Smoked Pork Sausage": 19,
  "Capsicum": 19,
  "Mushrooms": 19,
  "Onions": 19,
  "Pineapple": 10,
  "Red & Green Chilli": 19,
  "Tomato": 19
})[item]
app.post('/order', (req, res) => {

  let cart = req.session.cart || []
  let priceTotal = 0;
  priceTotal += mapPrice(req.body.crust)
  priceTotal += mapPrice(req.body.size)

  priceTotal += mapPrice(req.body.menu)
  req.body.topping = req.body.topping || [];
  req.body.topping.map((top) => {
    priceTotal += mapPrice(top)
  })
  cart.push( Object.assign({}, { topping: [] }, req.body,  { price: priceTotal } ))
  req.session.cart = cart
  console.log('cart', cart)
  res.render('checkout', { user: req.session.user, cart: req.session.cart });
});



app.get('/register', (req, res) => {
 
  res.render('register');
});

app.post('/register', (req, res) => {


  var sql = `INSERT INTO account (firstname, email, password) VALUES ('${req.body.firstname}','${req.body.email}','${req.body.password}')`;
  connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
  })

  req.session.user = {
    firstname: req.body.firstname,
    email: req.body.email,
    password: req.body.password
  };

  let message = "Register Successfully, Please login"
  console.log(req.body)
  res.render('login', { message })
})


app.get('/login', (req, res) => {
  res.render('login');
});

app.post('/login', (req, res) => {


  var sql = `SELECT * FROM account WHERE email='${req.body.email}' AND password='${req.body.password}'`;
  connection.query(sql, function (err, result) {
    if (err) throw err;
    if (result.length === 0) {
      let message = "Login fail"
      res.render('login', { message })

    } else {
      req.session.user = {
        id: result[0].id,
        firstname: req.body.firstname,
        email: req.body.email,
        password: req.body.password
      };
      user = req.session.user;

      res.redirect('/')

    }
  })
})

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
// [END app]